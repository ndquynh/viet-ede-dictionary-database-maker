import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

/**
 * Created by ndQ on 20/08/2016.
 */
public class RawSplitter {


    public static final String EDEWORD = "edeword";

    public static final String WORDTYPES = "wordtypes";
    public static final String TYPE = "type";

    public static final String MEANINGS = "meanings";
    public static final String VIETWORD = "vietword";
    public static final String SAMPLES = "samples";

    public static final String SAMPLE_EDE = "sample_ede";
    public static final String SAMPLE_VN = "sample_vn";
    Map<String, String> onlySamples = new HashMap<String, String>();

    Map<String, String> wordCategories;

    String[] alphabets = {"A", "À", "Á", "B", "{", "}", "D", "Â", "E", "E|", "Ã", "E$", "G", "H", "I", "&", "J", "K", "L", "M", "N", "~"};

    public RawSplitter() {

        wordCategories = new HashMap<String, String>();
        wordCategories.put("cd", "Ca dao");
        wordCategories.put("dt", "Danh tæì hoàûc nhæîng täø håüp tæång âæång");
        wordCategories.put("âgt", "Âäüng tæì hoàûc nhæîng täø håüp tæång âæång");
        wordCategories.put("ât", "Âaûi tæì hoàûc nhæîng täø håüp tæång âæång");
        wordCategories.put("lt", "Liãn tæì hoàûc nhæîng täø håüp tæång âæång");
        wordCategories.put("ng", "Nghéa");
        wordCategories.put("pht", "Phuû tæì");
        wordCategories.put("tht", "Thaïn tæì hoàûc nhæîng täø håüp tæång âæång");
        wordCategories.put("tng", "Thaình ngæî, tuûc ngæî");
        wordCategories.put("trt", "Tråü tæì");
        wordCategories.put("trgt", "Traûng tæì");
        wordCategories.put("tt", "Tênh tæì");
        wordCategories.put("tl", "Tæì loaûi");
        wordCategories.put("gt", "Giåïi tæì");
        wordCategories.put("st", "Säú tæì");
        wordCategories.put("Anh", "tiãúng Anh");
        wordCategories.put("Pr", "(Pràng) tiãúng Phaïp");
        wordCategories.put("Yu", "(Yuàn) tiãúng Viãût");
        wordCategories.put("La", "tiãúng Latinh");
        wordCategories.put("Trg", "tiãúng Trung Quäúc");

    }

    public JSONArray parseRawToJson(List<String> rawInput) {
        JSONArray wordArray = new JSONArray();
        int currentPos = 0;
//        Matcher matcher;
//        Pattern pattern;
        boolean foundMeaningNum = true;

        JSONObject currEdeWord = null;
        JSONArray wordTypes = null;
        JSONArray meanings = null;
        JSONArray samples;

        int missMatched = 0;

        int alphabetNo = 0;
        int illegal = 0;
        int needToTrim = 0;
        int edeWordNo = 0;
        int edeWordTrimmed = 0;
        int directMeanings = 0;

        int stt = 0;

        while (currentPos < rawInput.size()) {
            String curLine = rawInput.get(currentPos);

            if (curLine.length() == 0) {
                currentPos++;
                continue;
            }

            try {
                if (!curLine.startsWith("\u0009")) {
                    if (!curLine.equals(curLine.trim())) {
                        curLine = curLine.trim();
                        needToTrim++;
                    }
                    edeWordNo++;

                    currEdeWord = new JSONObject();
                    wordArray.put(currEdeWord);
                    currEdeWord.put("ID", stt++);
                    wordTypes = new JSONArray();

                    currEdeWord.put(WORDTYPES, wordTypes);

//                    meanings = new JSONArray();

                } else {
//                    ln("start with tab >" + curLine);
                }

                String[] splittedLine = curLine.split("\u0009+");

                switch (splittedLine.length) {
                    case 0: // empty line
                        ln("Empty line... " + curLine);
                        break;
                    case 1: {
                        alphabetNo++;
//                    ln(currentPos+1 + " Alphabet line... " + curLine);
                        // Alphabet letter

                        currEdeWord.put(EDEWORD, curLine.trim() + " ");

                        JSONObject typeJSON = new JSONObject();
                        wordTypes.put(typeJSON);
                        typeJSON.put(TYPE, "alphabet");
                        meanings = new JSONArray();
                        typeJSON.put(MEANINGS, meanings);

                        JSONObject aMeanJSon = new JSONObject();
                        aMeanJSon.put(VIETWORD, "Chæî caïi " + curLine.trim());
                        meanings.put(aMeanJSon);
                    }
                    break;
                    case 2:
                        /**
                         *  ede (type)[\u0009]meaning. samples:samples;samples:samples.
                         */
                        String edeW = splittedLine[0].trim();

                        if (edeW.trim().contains("Ah")) {
                            ln(curLine.toString());
                        }

                        if (edeW.length() > 0) {
                            edeWordTrimmed++;

                            //meaning
                            String meaning_sample = splittedLine[1].trim();
//                            String[] split = meaning_sample.split("\\.");
//                            l(" new ede word >" + edeW );

                            if (edeW.contains("/")) {
                                ln(edeW);
                            }
                            if (meaning_sample.startsWith("I.")) {
                                currEdeWord.put(EDEWORD, edeW);
                                String[] meaning_sample_arr = meaning_sample.split("\\.");
                                JSONObject aTypeJS = new JSONObject();
                                wordTypes.put(aTypeJS);

                                meanings = new JSONArray();
                                aTypeJS.put(MEANINGS, meanings);
                                boolean gotMeanings = false;
                                for (int i = 0; i < meaning_sample_arr.length; i++) {
                                    String targetStr = meaning_sample_arr[i].trim();
                                    if (targetStr.length() < 1)
                                        continue;
                                    if (i == 1) {
                                        String abbre = targetStr.substring(targetStr.indexOf('(') + 1, targetStr.indexOf(')'));
                                        String fullType = wordCategories.get(abbre);
                                        if (fullType == null)
                                            ln("not exist type " + abbre + " in " + meaning_sample);
                                        else {
                                            aTypeJS.put(TYPE, abbre);

                                            String meaningStr = targetStr.substring(targetStr.indexOf(')') + 1);
                                            if (!meaning_sample.trim().contains("1.")) {


                                                JSONObject aMeaningJSON = new JSONObject();
                                                aMeaningJSON.put(VIETWORD, meaningStr.trim());
                                                meanings.put(aMeaningJSON);
                                                gotMeanings = true;
                                                if (i + 1 == meaning_sample_arr.length) {
//                                                    ln(meaning_sample);
                                                    continue;
                                                }
                                                i++;
                                                String sampleStr = meaning_sample_arr[i];
                                                String[] samplesArr = sampleStr.split(";");
                                                JSONArray samplePairs = new JSONArray();
                                                aMeaningJSON.put(SAMPLES, samplePairs);
                                                try {
                                                    for (String sample : samplesArr) {
                                                        String[] samplePair = sample.split(":");

                                                        JSONObject aJSSample = new JSONObject();
                                                        aJSSample.put(SAMPLE_EDE, samplePair[0].trim());
                                                        aJSSample.put(SAMPLE_VN, samplePair[1].trim());

                                                        samplePairs.put(aJSSample);
                                                    }
                                                } catch (java.lang.ArrayIndexOutOfBoundsException e) {
                                                    ln(edeW);
//                                                e.printStackTrace();
                                                }
                                            } else {
//                                                ln("meaningStr.trim().contains(\"1.\") " + meaning_sample);
                                            }
                                        }
                                        continue;
                                    }
                                    if (i == 2) {
                                        if (meaning_sample.contains("1.")) {
                                            String meaningStr = meaning_sample_arr[2].trim();

                                            JSONObject aMeaningJSON = new JSONObject();
                                            aMeaningJSON.put(VIETWORD, meaningStr.trim());
                                            meanings.put(aMeaningJSON);
                                            gotMeanings = true;
                                            if (i + 1 == meaning_sample_arr.length) {
//                                                    ln(meaning_sample);
                                                continue;
                                            }
                                            i++;
                                            String sampleStr = meaning_sample_arr[i];
                                            String[] samplesArr = sampleStr.split(";");
                                            JSONArray samplePairs = new JSONArray();
                                            aMeaningJSON.put(SAMPLES, samplePairs);
                                            for (String sample : samplesArr) {
                                                String[] samplePair = sample.split(":");

                                                JSONObject aJSSample = new JSONObject();
                                                aJSSample.put(SAMPLE_EDE, samplePair[0].trim());
                                                aJSSample.put(SAMPLE_VN, samplePair[1].trim());

                                                samplePairs.put(aJSSample);
                                            }
//                                            ln(meaning_sample);
                                        } else {
                                            ln(meaning_sample);
                                        }
                                    }
                                }
                            } else {
                                String ede = edeW.substring(0, edeW.indexOf("(")).trim();
                                currEdeWord.put(EDEWORD, ede);
                                String typeStr = edeW.substring(edeW.indexOf("("));
                                String[] split = typeStr.trim().split("\\(");
                                JSONObject aTypeJS = new JSONObject();
                                wordTypes.put(aTypeJS);

                                if (split.length >= 2) {
                                    for (String str : split) {
                                        if (str.length() < 2)
                                            continue;
                                        String typeAbbre = str.substring(0, str.indexOf(")"));
                                        Set<String> strings = wordCategories.keySet();
                                        boolean found = false;
                                        for (String k : strings) {
                                            if (typeAbbre.toLowerCase().equals(k.toLowerCase())) {
//                                                l(ede + ">" + typeAbbre + "|");
                                                try {
                                                    String type = (String) aTypeJS.get(TYPE);
                                                    aTypeJS.put(TYPE, type + ", " + typeAbbre);
                                                } catch (JSONException e) {
                                                    aTypeJS.put(TYPE, typeAbbre);
                                                }
                                                found = true;
                                            }
                                        }
                                        if (!found) {
                                            aTypeJS.put(TYPE, typeAbbre);
                                        }
                                    }

//                                    ln("");
                                } else {
                                }

                                if (meaning_sample.startsWith("1")) {
//                                ln(" word meaning >" + meaning_sample);
                                    meanings = new JSONArray();
                                    aTypeJS.put(MEANINGS, meanings);
                                    String[] meaning_sample_arr = meaning_sample.split("\\.");
                                    for (int i = 0; i < meaning_sample_arr.length; i++) {
                                        String targetStr = meaning_sample_arr[i].trim();
                                        if (i == 1) {
                                            JSONObject aJsonMean = new JSONObject();
                                            meanings.put(aJsonMean);
//                                        jsonType.put(MEANINGS, meanings);
                                            aJsonMean.put(VIETWORD, targetStr);
                                            while (i + 1 < meaning_sample_arr.length) {
//                                                    ln(meaning_sample);

                                                i++;
                                                String sampleStr = meaning_sample_arr[i].trim();
                                                String[] samplesArr = sampleStr.split(";");
                                                if (sampleStr.trim().length() == 0)
                                                    continue;
                                                JSONArray samplePairs = new JSONArray();
                                                aJsonMean.put(SAMPLES, samplePairs);
                                                try {
                                                    for (String sample : samplesArr) {
                                                        String[] samplePair = sample.split(":");

                                                        JSONObject aJSSample = new JSONObject();
                                                        aJSSample.put(SAMPLE_EDE, samplePair[0].trim());
                                                        aJSSample.put(SAMPLE_VN, samplePair[1].trim());

                                                        samplePairs.put(aJSSample);
                                                    }
                                                } catch (java.lang.ArrayIndexOutOfBoundsException e) {
                                                    l(edeW);
                                                    e.printStackTrace();
                                                }
                                            }
                                        }
                                    }
                                } else {
//                                    ln(" normal case >" + meaning_sample);

                                    String[] meaning_sample_arr = meaning_sample.split("\\.");
                                    meanings = new JSONArray();
                                    aTypeJS.put(MEANINGS, meanings);
                                    for (int i = 0; i < meaning_sample_arr.length; i++) {
                                        String targetStr = meaning_sample_arr[i].trim();
                                        if (i == 0) {
                                            JSONObject aJsonMean = new JSONObject();
                                            meanings.put(aJsonMean);
//                                        jsonType.put(MEANINGS, meanings);
                                            aJsonMean.put(VIETWORD, targetStr);
                                            while (i + 1 < meaning_sample_arr.length) {
//                                                    ln(meaning_sample);
                                                i++;
                                                String sampleStr = meaning_sample_arr[i].trim();
                                                String[] samplesArr = sampleStr.split(";");
                                                if (sampleStr.trim().length() == 0)
                                                    continue;
                                                JSONArray samplePairs = new JSONArray();
                                                aJsonMean.put(SAMPLES, samplePairs);
                                                try {
                                                    for (String sample : samplesArr) {
                                                        String[] samplePair = sample.split(":");

                                                        JSONObject aJSSample = new JSONObject();
                                                        aJSSample.put(SAMPLE_EDE, samplePair[0].trim());
                                                        aJSSample.put(SAMPLE_VN, samplePair[1].trim());

                                                        samplePairs.put(aJSSample);
                                                    }
                                                } catch (java.lang.ArrayIndexOutOfBoundsException e) {
                                                    l(edeW);
                                                    e.printStackTrace();
                                                }
                                            }
                                        }
                                    }
                                }
                            }


                        } else {

                            String sub_Type_Meaning = splittedLine[1].trim();
                            // sub word-type
                            if (sub_Type_Meaning.startsWith("II")) {
                                JSONObject aTypeJS = new JSONObject();
                                wordTypes.put(aTypeJS);

//                                ln(splittedLine[0] + " -> subtype " + sub_Type_Meaning);

                                String[] sub_type_arr = sub_Type_Meaning.split("\\.");
                                meanings = new JSONArray();
                                aTypeJS.put(MEANINGS, meanings);
                                JSONObject aMeaningJS = null;
                                for (int i = 1; i < sub_type_arr.length; i++) {
                                    String str = sub_type_arr[i].trim();
                                    if (i == 1) {
                                        aMeaningJS = new JSONObject();
                                        meanings.put(aMeaningJS);
                                        aTypeJS.put(TYPE, str.substring(str.indexOf('(') + 1, str.indexOf(')')));
                                        if (str.substring(str.indexOf(')')).contains("1")) {
                                            i++;
                                            aMeaningJS.put(VIETWORD, sub_type_arr[i].trim());

                                        } else {
                                            aMeaningJS.put(VIETWORD, str.substring(str.indexOf(')') + 1).trim());
                                        }
                                    } else {

                                        String sampleStr = str.trim();
                                        String[] samplesArr = sampleStr.split(";");
                                        if (sampleStr.trim().length() == 0)
                                            continue;
                                        JSONArray samplePairs = new JSONArray();
                                        aMeaningJS.put(SAMPLES, samplePairs);
                                        try {
                                            for (String sample : samplesArr) {
                                                String[] samplePair = sample.split(":");

                                                JSONObject aJSSample = new JSONObject();
                                                aJSSample.put(SAMPLE_EDE, samplePair[0].trim());
                                                aJSSample.put(SAMPLE_VN, samplePair[1].trim());

                                                samplePairs.put(aJSSample);
                                            }
                                        } catch (java.lang.ArrayIndexOutOfBoundsException e) {
                                            l(edeW);
                                            e.printStackTrace();
                                        }
                                    }

//                                    ln(currEdeWord.toString());
                                }
                            } else if (sub_Type_Meaning.startsWith("1.") || sub_Type_Meaning.startsWith("2.") || sub_Type_Meaning.startsWith("3.") || sub_Type_Meaning.startsWith("4.") || sub_Type_Meaning.startsWith("5.") || sub_Type_Meaning.startsWith("6.")) {

                                String[] sub_Type_Meaning_arr = sub_Type_Meaning.split("\\.");
                                JSONObject aMeaningJs = new JSONObject();

                                for (int i = 1; i < sub_Type_Meaning_arr.length; i++) {
                                    String str = sub_Type_Meaning_arr[i].trim();
                                    if (i == 1) {

                                        if (str.contains(":")) { //there is only sample
                                            //
                                            ln(sub_Type_Meaning.toString());
                                            onlySamples.put(sub_Type_Meaning.toString().split("\\.")[1], "+++");

                                        } else {
                                            aMeaningJs.put(VIETWORD, str);
                                        }
                                    } else {
                                        meanings.put(aMeaningJs);
                                        String sampleStr = str.trim();
                                        String[] samplesArr = sampleStr.split(";");
                                        if (sampleStr.trim().length() == 0)
                                            continue;
                                        JSONArray samplePairs = new JSONArray();
                                        aMeaningJs.put(SAMPLES, samplePairs);
                                        try {
                                            for (String sample : samplesArr) {
                                                String[] samplePair = sample.split(":");

                                                JSONObject aJSSample = new JSONObject();
                                                aJSSample.put(SAMPLE_EDE, samplePair[0].trim());
                                                aJSSample.put(SAMPLE_VN, samplePair[1].trim());

                                                samplePairs.put(aJSSample);
                                            }
                                        } catch (java.lang.ArrayIndexOutOfBoundsException e) {
                                            l(edeW);
                                            e.printStackTrace();
                                        }
                                    }
                                }

                            } else {
                                //Exception
                                ln(splittedLine[0] + " -> sub " + splittedLine[1].trim());
                            }


                            // sub meaning
//                            ln( splittedLine[0] + " -> sub " + splittedLine[1].trim());
                        }


                        break;
                    default:
                        ln(++illegal + " Illegal line... " + curLine);
                        l("  >>>");
                        for (String st : splittedLine) {
                            l(st + "|");
                        }
                        ln("");


                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            currentPos++;
        }

        Set<String> keys = onlySamples.keySet();
        for (String content : keys) {
            try {
                String wordType = onlySamples.get(content);
                String[] split = content.split(";");
                for (String str : split) {
                    String[] aPair = str.split(":");

                    currEdeWord = new JSONObject();
                    wordArray.put(currEdeWord);
                    currEdeWord.put(EDEWORD, aPair[0]);

                    wordTypes = new JSONArray();
                    currEdeWord.put(WORDTYPES, wordTypes);
                    JSONObject aTypeJS = new JSONObject();

                    wordTypes.put(aTypeJS);
                    aTypeJS.put(TYPE, wordType);

                    meanings = new JSONArray();
                    aTypeJS.put(MEANINGS, meanings);

                    JSONObject aMeaningJS = new JSONObject();
                    meanings.put(aMeaningJS);
                    aMeaningJS.put(VIETWORD, aPair[1]);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        ln("Need to trim line No: 864 " + needToTrim);
        ln("Ede word size No: 3984 " + edeWordNo);
        ln("Alphabet No: 37 " + alphabetNo);
        ln("Ede word edeWord trimmed No: 3947 " + edeWordTrimmed);

        return wordArray;
    }


    void ln(String log) {
        System.out.println(log);
    }

    void l(String log) {
        System.out.print(log);
    }

    public static void main(String[] args) {

        RawDataHandler handler = new RawDataHandler();
        ArrayList<String> strings = handler.readRaw(null);
//        for (String line : strings) {
//            System.out.println(line);
//        }

        RawSplitter spliiter = new RawSplitter();


        JSONArray jsonArray = spliiter.parseRawToJson(strings);

//        System.out.println(jsonArray.toString());

        spliiter.createQueriableDatabaseFields(jsonArray);

//        spliiter.printDictionary(dict);

    }

    private void printDictionary(Map<String, String> dict) {

        Set<String> keys = dict.keySet();

        int count = 0;
        for (String key : keys) {
            ln(++count + " " + key + " -> " + dict.get(key));
        }

    }

    private void createQueriableDatabaseFields(JSONArray jsonArray) {
        Map<String, String> ede_Viet_Dict = new HashMap<String, String>();
        Map<String, String> viet_Ede_Dict = new HashMap<String, String>();

        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = null;
            try {
                jsonObject = jsonArray.getJSONObject(i);
                importEdeWord(ede_Viet_Dict, jsonObject, viet_Ede_Dict);

            } catch (JSONException e) {
                ln(jsonObject.toString());
                e.printStackTrace();
            }
        }

//        printDictionary(ede_Viet_Dict);
//        printDictionary(viet_Ede_Dict);


        makeDatabase(ede_Viet_Dict, viet_Ede_Dict);


    }


    final String DB_PATH = System.getProperty("user.dir").concat("\\Desktop\\EDEVIET_db\\ede-viet-dictionary.db");

    final String TYPE_OPEN = "<span class='category'>";
    final String TYPE_CLOSE = "</span>";

    final String MEANING_UL_OPEN = "<ul type='1'>";
    final String MEANING_UL_CLOSE = "</ul>";

    final String MEANING_IL_OPEN = "<li class='meaning'>";
    final String MEANING_IL_CLOSE = "</li>";

    final String SAMPLE_UL_OPEN = "<ul style='list-style-type:circle'>";
    final String SAMPLE_UL_CLOSE = "</ul>";

    final String SAMPLE_IL_OPEN = "<li class='sample'>";
    final String SAMPLE_IL_CLOSE = "</li>";
    final String SAMPLE_SEPARATOR = ": ";

    private void importEdeWord(Map<String, String> ede_Viet_Dict, JSONObject jsonObject, Map<String, String> viet_Ede_Dict) throws JSONException {

        String edeWord = jsonObject.getString(EDEWORD);

        String content = makeEdeWordContent(jsonObject, viet_Ede_Dict);

        ede_Viet_Dict.put(edeWord, content);
    }

    private String makeEdeWordContent(JSONObject jsonObject, Map<String, String> viet_Ede_Dict) throws JSONException {
        StringBuilder content = new StringBuilder();

        JSONArray wordTypeArrJS = jsonObject.getJSONArray(WORDTYPES);

        for (int a = 0; a < wordTypeArrJS.length(); a++) {
            JSONObject aTypeJS = wordTypeArrJS.getJSONObject(a);
            content.append(TYPE_OPEN);
            String typeStr = aTypeJS.getString(TYPE);

            String category = wordCategories.get(typeStr);
            content.append(category != null ? category : typeStr);
            content.append(TYPE_CLOSE);

            content.append(MEANING_UL_OPEN);


            JSONArray meaningArrJS = aTypeJS.getJSONArray(MEANINGS);

            for (int b = 0; b < meaningArrJS.length(); b++) {

                JSONObject aMeaningJS = meaningArrJS.getJSONObject(b);
                try {
                    String vietWordMeaning = aMeaningJS.getString(VIETWORD);

                    importVietWord(jsonObject.getString(EDEWORD), category != null ? category : typeStr, vietWordMeaning, aMeaningJS, viet_Ede_Dict);

                    content.append(MEANING_IL_OPEN);
                    content.append(vietWordMeaning);
                } catch (JSONException e) {
                    ln("meaning " + jsonObject.getString(EDEWORD) + " - " + jsonObject.toString());
                }
                try {
                    JSONArray sampleArrJS = aMeaningJS.getJSONArray(SAMPLES);
                    if (sampleArrJS.length() > 0) {

                        content.append(SAMPLE_UL_OPEN);

                        for (int c = 0; c < sampleArrJS.length(); c++) {
                            JSONObject aSamplePairJS = sampleArrJS.getJSONObject(c);
                            content.append(SAMPLE_IL_OPEN);

                            String edeSample = aSamplePairJS.getString(SAMPLE_EDE);
                            content.append(createSampleUriFormat(edeSample));

                            content.append(SAMPLE_SEPARATOR);

                            String vnSample = aSamplePairJS.getString(SAMPLE_VN);
                            content.append(createTranslatedFormat(vnSample));
                            content.append(SAMPLE_IL_CLOSE);
                        }

                        content.append(SAMPLE_UL_CLOSE);
                    }
                } catch (JSONException e) {

                }
                content.append(MEANING_IL_CLOSE);
            }

            content.append(MEANING_UL_CLOSE);

        }
        return content.toString();
    }

    private void importVietWord(String edeWord, String category, String vietWordMeaning, JSONObject meaningJsonObject, Map<String, String> viet_ede_dict) {
        try {
//            int left_Parenthesis_Pos = vietWordMeaning.indexOf('(');
            if(!vietWordMeaning.contains("(")) {
                String[] split = vietWordMeaning.split(",");
                for(String mean : split) {
                    viet_ede_dict.put(mean, makeVietWordContent(edeWord, category, meaningJsonObject));
                }
            } else {
//            int comma_Pos = vietWordMeaning.indexOf(',');

                viet_ede_dict.put(vietWordMeaning, makeVietWordContent(edeWord, category, meaningJsonObject));
            }

        } catch (JSONException e) {
            ln(vietWordMeaning);
            e.printStackTrace();
        }
    }

    private String makeVietWordContent(String edeWord, String category, JSONObject meaningJsonObject) throws JSONException {

        StringBuilder content = new StringBuilder();

        content.append(TYPE_OPEN);

        content.append(category);
        content.append(TYPE_CLOSE);

        content.append(MEANING_UL_OPEN);

        content.append(MEANING_IL_OPEN);
        content.append(edeWord);
        try {
            JSONArray sampleArrJS = meaningJsonObject.getJSONArray(SAMPLES);
            if (sampleArrJS.length() > 0) {

                content.append(SAMPLE_UL_OPEN);

                for (int c = 0; c < sampleArrJS.length(); c++) {
                    JSONObject aSamplePairJS = sampleArrJS.getJSONObject(c);
                    content.append(SAMPLE_IL_OPEN);

                    String vnSample = aSamplePairJS.getString(SAMPLE_VN);
                    content.append(createSampleUriFormat(vnSample));

                    content.append(SAMPLE_SEPARATOR);

                    String edeSample = aSamplePairJS.getString(SAMPLE_EDE);
                    content.append(createTranslatedFormat(edeSample));
                    content.append(SAMPLE_IL_CLOSE);
                }

                content.append(SAMPLE_UL_CLOSE);
            }
        } catch (JSONException e) {

        }
        content.append(MEANING_IL_CLOSE);

        content.append(MEANING_UL_CLOSE);

        return content.toString();
    }

    private String createTranslatedFormat(String vnSample) {
        StringBuilder result = new StringBuilder();
        final String OPEN_SPAN_TAG = "<span class='translated'>";
        final String CLOSE_SPAN_TAG = "</span>";
        result.append(OPEN_SPAN_TAG);
        result.append(vnSample);
        result.append(CLOSE_SPAN_TAG);

        return result.toString();
    }

    private String createSampleUriFormat(String edeSample) {
        StringBuilder result = new StringBuilder();
        String[] sampleEntries = edeSample.split(" ");
        final String OPEN_SPAN_TAG = "<span class='example'>";
        final String CLOSE_SPAN_TAG = "</span>";

        final String OPEN_A_TAG = "<a href='entry://";
        final String CLASS = "' class='entry'>";

        final String CLOSE_A_TAG = "</a> ";


        result.append(OPEN_SPAN_TAG);
        for (String entry : sampleEntries) {
            result.append(OPEN_A_TAG);
            result.append(entry);
            result.append(CLASS);
            result.append(entry);
            result.append(CLOSE_A_TAG);
        }

        result.append(CLOSE_SPAN_TAG);

        return result.toString();
    }






    public static final String TABLE_WORDS = "words";
    public static final String TABLE_WORDS_FTS = "words_fts";

    public static final String COL_ID = "id";
    public static final String COL_WORD = "word";
    public static final String COL_CONTENT = "content";
    public static final String COL_SOUND_PATH = "sound_path";
    public static final String COL_FAVOURITE = "favourite";
    public static final String COL_LANGUAGE = "language";


    public static final String DROP_TABLE_WORDS = "DROP TABLE IF EXISTS " + TABLE_WORDS;
    public static final String CREATE_TABLE_WORDS = "CREATE TABLE " + TABLE_WORDS + " ( "
            + COL_ID + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, "
            + COL_WORD + " TEXT NOT NULL, "
            + COL_CONTENT + " TEXT, "
            + COL_SOUND_PATH + " TEXT, "
            + COL_FAVOURITE + " INTEGER DEFAULT 0, "
            + COL_LANGUAGE + " VARCHAR(128))";

    public static final String DROP_TABLE_WORDS_FTS = "DROP TABLE IF EXISTS " + TABLE_WORDS_FTS;

    public static final String CREATE_TABLE_WORDS_FTS = "CREATE VIRTUAL TABLE " + TABLE_WORDS_FTS + " USING fts4 ( "
//            + COL_ID + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, "
            + COL_WORD + " TEXT NOT NULL, "
            + COL_CONTENT + " TEXT, "
            + COL_SOUND_PATH + " TEXT, "
            + COL_FAVOURITE + " INTEGER DEFAULT 0, "
            + COL_LANGUAGE + " VARCHAR(128))";

    public static final String INSERT = "INSERT INTO " + TABLE_WORDS + " ("
            + COL_WORD + ", "
            + COL_CONTENT + ", "
            + COL_SOUND_PATH + ", "
            + COL_FAVOURITE + ", "
            + COL_LANGUAGE + ""
            + ") VALUES (\"%s\", \"%s\", \"%s\", \"%s\", \"%s\")";

    public static final String INSERT_FTS = "INSERT INTO " + TABLE_WORDS_FTS + " ("
            + COL_WORD + ", "
            + COL_CONTENT + ", "
            + COL_SOUND_PATH + ", "
            + COL_FAVOURITE + ", "
            + COL_LANGUAGE + ""
            + ") VALUES (\"%s\", \"%s\", \"%s\", \"%s\", \"%s\")";


    private void makeDatabase(Map<String, String> ede_viet_dict, Map<String, String> viet_ede_dict) {
        Connection connection = null;
        try {

            File dbFile = new File(DB_PATH);
            try {
                if(!dbFile.createNewFile()) {
                    l("Create DB file failed " + DB_PATH);
                    return;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            connection = DriverManager.getConnection("jdbc:sqlite:" + DB_PATH);
            Statement statement = connection.createStatement();
            statement.setQueryTimeout(30);


            int err_count = 0;

            statement.executeUpdate(DROP_TABLE_WORDS);
            statement.executeUpdate(DROP_TABLE_WORDS_FTS);
//            statement.executeUpdate(CREATE_TABLE_WORDS);
            statement.executeUpdate(CREATE_TABLE_WORDS_FTS);

            Set<String> edeKeys = ede_viet_dict.keySet();
            for (String key : edeKeys) {
                try {
//                    statement.executeUpdate(String.format(INSERT, key, ede_viet_dict.get(key), "", "0", "ede"));
                    statement.executeUpdate(String.format(INSERT_FTS, key, ede_viet_dict.get(key), "", "0", "ede"));
                } catch (SQLException e) {
//                    ln(++err_count + " " + key);
                    ln(String.format(INSERT_FTS, key, ede_viet_dict.get(key), "", "0", "ede"));
//                     ln(key + " -> " + ede_viet_dict.get(key));
//                    e.printStackTrace();
                }
            }

            Set<String> vietKeys = viet_ede_dict.keySet();
            for (String key : vietKeys) {
                try {
//                    statement.executeUpdate(String.format(INSERT, key, viet_ede_dict.get(key), "", "", "viet"));
                    statement.executeUpdate(String.format(INSERT_FTS, key, viet_ede_dict.get(key), "", "", "viet"));
                } catch (SQLException e) {
//                    ln(++err_count + " " + key);
                    ln(String.format(INSERT_FTS, key, viet_ede_dict.get(key), "", "", "ede"));
//                    ln(key + " -> " + viet_ede_dict.get(key));
//                    e.printStackTrace();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (connection != null)
                    connection.close();
            } catch (SQLException e) {
                // connection close failed.
                System.err.println(e);
            }
        }

        ln("Create database completed");

    }


}
