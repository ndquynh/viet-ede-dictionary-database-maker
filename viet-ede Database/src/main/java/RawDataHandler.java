import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ndQ on 20/08/2016.
 */
public class RawDataHandler {


    final String EDEWORD = "edeword";

    final String WORDTYPES = "wordtypes";
    final String TYPE = "type";

    final String MEANINGS = "meanings";
    final String VIETWORD = "vietword";
    final String SAMPLES = "samples";

    final String SAMPLE_EDE = "sample_ede";
    final String SAMPLE_VN = "sample_vn";


    ArrayList<String> readRaw(File file) {

        ArrayList<String> raw = new ArrayList<String>();
        String line;
        InputStream resourceAsStream = getClass().getClassLoader().getResourceAsStream("raw/ede-vn.txt");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(resourceAsStream));
        try {
            while ((line = bufferedReader.readLine()) != null) {
                raw.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return raw;
    }

    public JSONArray parseRawToJson(List<String> rawInput) {
        JSONArray wordArray = new JSONArray();
        int currentPos = 0;
        Matcher matcher;
        Pattern pattern;
        boolean foundMeaningNum = true;

        JSONObject word = null;
        JSONArray wordTypes = null;
        JSONArray meanings = null;
        JSONArray samples;

        int missMatched = 0;

        while (currentPos < rawInput.size()) {
            boolean catched = false;

            String line = rawInput.get(currentPos).trim();

            if (line.trim().length() == 0) {
                currentPos++;
                continue;
            }


            /*
            TODO: All line cases

            A| TODO: Alphabet letter

            Yæp yæp (tt)	Roïn reïn. Yæp yæp ]huang mu\t: Roïn reïn bæåïc vaìo. TODO: only edeword, 1 wordtype, 1 meaning, 1 or multiple samples

            Yæl â^ (âgt)	Náng lãn. TODO: only edeword, 1 wordtype, 1 meaning

            Yæ\ 	I. (dt) Loaûi låì bàòng äúng läö ä cheí 1 âáöu âan toe miãûng phãùu, coï hom, âàût åí chäù næåïc chaíy âãø âoïn bàõt caï. TODO: edeword, multiple wordtype, 1 meaning, 0 or multiple samples
                    II. (tt). 1. Vãö. Adiã leh yæ\ tlam: Tråìi âaî vãö chiãöu; Yæ\ mduän: Vãö giaì. TODO: multiple wordtype, 1 meaning, 0 or multiple samples
                            2. Heïo. Am^ khua ama yæ\: Cha giaì meû giaì (heïo). TODO: previous wordtype, 1 meaning, 0 or multiple samples
                            3. Lëm. P^t yæ\ he\: Nguí lëm âi. TODO: previous wordtype, 1 meaning, 0 or multiple samples
                    III.(tt) Giäúng caïi (tæì duìng chè âäüng váût giäúng caïi). U|n ana: Låün naïi; Mnu\ ana: Gaì maïi. TODO: another wordtype but 1 meaning,

            Yå\ng 	I. (âgt) 1. Run, rung, quay, xoay. TODO: Multiple wordtype at first line, multiple meaning.


            Yæh yæp (tt)	1. Cháûm chaûp. Ãbat yæh yæp: Âi cháûm chaûp. TODO: Multiple meaning at first line. There is 1 or multiple samples

            Bi	1. (lt) Coìn. Káo nao mà bruà bi anak káo dã nao sang hrà: Täi âi laìm coìn con täi âi hoüc.TODO: Multiple meaning at first line. There is 1 or multiple samples


            Ai ãwa (dt) (tt)	Khê thãú, håi sæïc. Phung l^ng kahan rue# nao ho\ng bo\ ai ãwa: Âoaìn quán bæåïc âi âáöy khê thãú; Amáo lo\ máo ai ãwa: Khäng coìn håi sæïc. TODO: Multiple wordtype with the same meaning. There is 1 or multiple samples

            Ale\, Alue# (âgt)	Nhaí.  Ale\ djah ãhàng: Nhaí baî tráöu. TODO: Multiple edeword with the same wordtype and meaning at first line. There is 1 or multiple samples



            */

            try {

                //A| TODO: Alphabet letter
                if (line.trim().length() <= 3) {
                    word = new JSONObject();
                    wordArray.put(word);
                    wordTypes = new JSONArray();
                    word.put(WORDTYPES, wordTypes);
                    meanings = new JSONArray();
//                    pushAttrToWord(matcher, word, wordTypes, meanings);
                    word.put(EDEWORD, line.trim());
                    JSONObject type = new JSONObject();
                    wordTypes.put(type);
                    type.put(MEANINGS, meanings);
                    JSONObject mean = new JSONObject();
                    mean.put(VIETWORD, "Chæî caïi " + line.trim());
                    meanings.put(mean);


                    currentPos++;
                    continue;
                }

                //Yæp yæp (tt)	Roïn reïn. Yæp yæp ]huang mu\t: Roïn reïn bæåïc vaìo. TODO: only edeword, 1 wordtype, 1 meaning, 1 or multiple samples
                //Aguah (traûng tæì)	Buäøi saïng. Aguah æm: Saïng såïm; Aguah mgi: Saïng mai; Aguah mbruã: Saïng häm qua.
                //{ap (dt)	Caïi vê. {ap dæm pràk: Caïi vê boí tiãön; {ap djà ti kngan	Vê xaïch tay.
                //Ya djo\ (tt)   Viãûc gç. Klei anei ya djo\ kå `u: Viãûc naìy viãûc gç âãún noï.
                //TODO 1 nghia, co the co nhieu vi du
                pattern = Pattern.compile("([\\u0020-\\u0027\\u002A-\\u007E\\u2019\\u00C0-\\u00FF]+)\\s*\\(([\\u0020\\u0040-\\u007E\\u2019\\u00C0-\\u00FF]+)\\)\\u0020*\\t+([^\\u0030-\\u0039][\\u0020-\\u007E\\u2019\\u00C0-\\u00FF]+)\\.((?:[\\u0020-\\u007E\\u2019\\u00C0-\\u00FF]+[:\t][\\u0020-\\u007E\\u2019\\u00C0-\\u00FF]+[;\\.\\u0020\\t]*)+)");
                matcher = pattern.matcher(line);
                if (matcher.matches()) {
                    catched = true;

                    word = new JSONObject();
                    wordArray.put(word);
                    wordTypes = new JSONArray();
                    word.put(WORDTYPES, wordTypes);
                    meanings = new JSONArray();
                    pushAttrToWord(matcher, word, wordTypes, meanings);

                    currentPos++;
                    continue;
                }

                //Yæl â^ (âgt)	Náng lãn. TODO: only edeword, 1 wordtype, 1 meaning
                //Aduän (dt)	Baì (näüi, ngoaûi).
                //Bi liã (âgt)	Tiãu, xaìi
                //Wai tã (dt)	Ve sáöu (ve thaïng 3).
                pattern = Pattern.compile("([\\u0020-\\u002F\\u003C-\\u007E\\u2019\\u00C0-\\u00FC]+)\\u0020*\\(([\\u0020\\u0040-\\u007E\\u2019\\u00C0-\\u00FC]+)\\)\\u0020*\\t+([\\u0020-\\u002F\\u003C-\\u007E\\u2019\\u00C0-\\u00FC]+(?:[\\u0020-\\u007E\\u2019\\u00C0-\\u00FC]+)?)\\.?[\\u0020\\t]*");// 1 nghia, ko co vi du
                matcher = pattern.matcher(line);
                if (matcher.matches()) {
                    catched = true;

                    word = new JSONObject();
                    wordArray.put(word);
                    wordTypes = new JSONArray();
                    word.put(WORDTYPES, wordTypes);
                    meanings = new JSONArray();
                    pushAttrToWord(matcher, word, wordTypes, meanings);

                    currentPos++;
                    continue;
                }


                //Yæ\ 	I. (dt) Loaûi låì bàòng äúng läö ä cheí 1 âáöu âan toe miãûng phãùu, coï hom, âàût åí chäù næåïc chaíy âãø âoïn bàõt caï.
                // Wàt 	I. (dt) Chim cuït.
                // TODO: edeword, multiple wordtype, 1 meaning, 0 samples
                pattern = Pattern.compile("([\\u0020-\\u002F\\u003C-\\u007E\\u2019\\u00C0-\\u00FC]+)\\s*\\tI{1,3}\\.\\u0020*\\(([\\u0040-\\u007E\\u2019\\u00C0-\\u00FC]+)\\)\\s*([\\u0020-\\u0039\\u003C-\\u007E\\u2019\\u00C0-\\u00FC]+)\\.\\u0020*");// nhieu nghia, ko co vidu
                matcher = pattern.matcher(rawInput.get(currentPos));
                if (matcher.matches()) {
                    catched = true;

                    word = new JSONObject();
                    wordArray.put(word);
                    wordTypes = new JSONArray();
                    word.put(WORDTYPES, wordTypes);
                    meanings = new JSONArray();

                    pushAttrToWord(matcher, word, wordTypes, meanings);

                    currentPos++;
                    continue;
                }


                //Ana 	I.(dt) Cáy. Ana kneh: Cáy mêt. Ana truäl: Cáy äøi.

                // TODO: edeword, multiple wordtype, 1 meaning, 1 or multiple samples
                pattern = Pattern.compile("([\\u0020-\\u002F\\u003C-\\u007E\\u2019\\u00C0-\\u00FC]+)\\s*\\tI{1,3}\\.\\u0020*\\(([\\u0040-\\u007E\\u2019\\u00C0-\\u00FC]+)\\)\\s*([\\u0020-\\u0039\\u003C-\\u007E\\u2019\\u00C0-\\u00FC]+)\\.\\u0020?((?:[\\u0020-\\u0039\\u003C-\\u007E\\u2019\\u00C0-\\u00FC]+[:\\u0020]+[\\u0020-\\u0039\\u003C-\\u007E\\u2019\\u00C0-\\u00FC]+[;\\.\\u0020]+?)+)");// nhieu nghia, co the co nhieu vi du
                matcher = pattern.matcher(rawInput.get(currentPos));
                if (matcher.matches()) {
                    catched = true;

                    word = new JSONObject();
                    wordArray.put(word);
                    wordTypes = new JSONArray();
                    word.put(WORDTYPES, wordTypes);
                    meanings = new JSONArray();

                    pushAttrToWord(matcher, word, wordTypes, meanings);

                    currentPos++;
                    continue;
                }


                //II. (tt). 1. Vãö. Adiã leh yæ\ tlam: Tråìi âaî vãö chiãöu; Yæ\ mduän: Vãö giaì. TODO: multiple wordtype, 1 meaning, 0 or multiple samples
                pattern = Pattern.compile("\\u0009*I{1,3}[\\.\\u0020]+\\(([\\u0040-\\u007E\\u2019\\u00C0-\\u00FC]+)\\)[\\s\\.]+\\d[\\s\\.]+([\\u0020-\\u007E\\u2019\\u00C0-\\u00FC]+)\\.((?:[\\u0020-\\u007E\\u2019\\u00C0-\\u00FC]+:[\\u0020-\\u007E\\u2019\\u00C0-\\u00FC]+[;\\.\\u0020]?)+)");// nhieu nghia, co the co nhieu vi du
                matcher = pattern.matcher(rawInput.get(currentPos));
                if (matcher.matches()) {
                    catched = true;


//                    pushAttrToWord(matcher, word, wordTypes, meanings);

//                    word.put(EDEWORD, matcher.group(1));

                    JSONObject wordType = new JSONObject();
                    wordTypes.put(wordType);

                    wordType.put(TYPE, matcher.group(1));
                    wordType.put(MEANINGS, meanings);

                    JSONObject meaning = new JSONObject();
                    meanings.put(meaning);

                    meaning.put(VIETWORD, matcher.group(2));

                    if (matcher.group(3) != null) {
                        String examples = matcher.group(3);
                        String[] arr = examples.split(";");

                        JSONArray sampleArray = new JSONArray();
                        for (String ex : arr) {
                            String[] samplePair = ex.split(":");
                            if (matcher.matches()) {
                                JSONObject aSample = new JSONObject();
                                aSample.put(SAMPLE_EDE, samplePair[0]);
                                aSample.put(SAMPLE_VN, samplePair[1]);
                                sampleArray.put(aSample);
                            }
                        }
                        meaning.put(SAMPLES, sampleArray);
                    }


                    currentPos++;
                    continue;
                }

                //III.(tt) Giäúng caïi (tæì duìng chè âäüng váût giäúng caïi). U|n ana: Låün naïi; Mnu\ ana: Gaì maïi.
                //II.(âaûi tæì chè âënh) Âoï, âáúy. Hlàk anàn: Häöi âoï. Må\ng anei kå anàn amáo kbæi äh: Tæì âáy âãún âoï khäng xa.
                // TODO: another wordtype but 1 meaning, with 1+ samples
                pattern = Pattern.compile("\\s*I{1,3}[\\.\\u0020]*\\(([\\u0020\\u0040-\\u007E\\u2019\\u00C0-\\u00FC]+)\\)[\\s\\.]+([\\u0020-\\u0039\\u003C-\\u007E\\u2019\\u00C0-\\u00FC]+)\\.((?:[\\u0020-\\u007E\\u2019\\u00C0-\\u00FC]+:[\\u0020-\\u007E\\u2019\\u00C0-\\u00FC]+[;\\.\\u0020]?)+)");
                matcher = pattern.matcher(rawInput.get(currentPos));
                if (matcher.matches()) {
                    catched = true;

                    JSONObject wordType = new JSONObject();
                    wordTypes.put(wordType);

                    wordType.put(TYPE, matcher.group(1));
                    wordType.put(MEANINGS, meanings);

                    JSONObject meaning = new JSONObject();
                    meanings.put(meaning);

                    meaning.put(VIETWORD, matcher.group(2));

                    if (matcher.group(3) != null) {
                        String examples = matcher.group(3);
                        String[] arr = examples.split(";");

                        JSONArray sampleArray = new JSONArray();
                        for (String ex : arr) {
                            String[] samplePair = ex.split(":");
                            if (matcher.matches()) {
                                JSONObject aSample = new JSONObject();
                                aSample.put(SAMPLE_EDE, samplePair[0]);
                                aSample.put(SAMPLE_VN, samplePair[1]);
                                sampleArray.put(aSample);
                            }
                        }
                        meaning.put(SAMPLES, sampleArray);
                    }


                    currentPos++;
                    continue;
                }


                //                III. (âgt) Cuïng (sæïc khoeí cho ngæåìi).
                // TODO: another wordtype but 1 meaning, with 0 samples
                pattern = Pattern.compile("\\s*I{1,3}[\\.\\u0020]+\\(([\\u0020\\u0040-\\u007E\\u2019\\u00C0-\\u00FC]+)\\)[\\.\\u0020]+([\\u0020-\\u0039\\u003C-\\u007E\\u2019\\u00C0-\\u00FC]+)\\.*\\s*");// nhieu nghia, co the co nhieu vi du
                matcher = pattern.matcher(rawInput.get(currentPos));
                if (matcher.matches()) {
                    catched = true;

                    JSONObject wordType = new JSONObject();
                    wordTypes.put(wordType);

                    wordType.put(TYPE, matcher.group(1));
                    wordType.put(MEANINGS, meanings);

                    JSONObject meaning = new JSONObject();
                    meanings.put(meaning);

                    meaning.put(VIETWORD, matcher.group(2));

                    if (matcher.group(3) != null) {
                        String examples = matcher.group(3);
                        String[] arr = examples.split(";");

                        JSONArray sampleArray = new JSONArray();
                        for (String ex : arr) {
                            String[] samplePair = ex.split(":");
                            if (matcher.matches()) {
                                JSONObject aSample = new JSONObject();
                                aSample.put(SAMPLE_EDE, samplePair[0]);
                                aSample.put(SAMPLE_VN, samplePair[1]);
                                sampleArray.put(aSample);
                            }
                        }
                        meaning.put(SAMPLES, sampleArray);
                    }


                    currentPos++;
                    continue;
                }


                //Yå\ng 	I. (âgt) 1. Run, rung, quay, xoay.
                //Blah 	I.(âgt)1. Bäø. Blah djuh: Bäø cuíi
                pattern = Pattern.compile("([\\u0020-\\u007E\\u2019\\u00C0-\\u00FC]+)\\tI{1,3}[\\u0020\\.]+\\(([\\u0040-\\u007E\\u2019\\u00C0-\\u00FC]+)\\)[\\u0020\\.]*\\d[\\u0020\\.]+([\\u0020-\\u007E\\u2019\\u00C0-\\u00FC]+)[\\u0020\\.]+((?:[\\u0020-\\u007E\\u2019\\u00C0-\\u00FC]+:[\\u0020-\\u007E\\u2019\\u00C0-\\u00FC]+[;\\.\\u0020]?)+)*");// nhieu nghia, co the co nhieu vi du
                matcher = pattern.matcher(rawInput.get(currentPos));
                if (matcher.matches()) {
                    catched = true;

                    word = new JSONObject();
                    wordArray.put(word);
                    wordTypes = new JSONArray();
                    word.put(WORDTYPES, wordTypes);
                    meanings = new JSONArray();

                    pushAttrToWord(matcher, word, wordTypes, meanings);

                    currentPos++;
                    continue;
                }


                //Boh jing (dt)	1. Tênh tæì.
                //Ha\ (phuû tæì)	1. Naìy, âáy (chè sæû hiãûn diãûn cuía váût).
                //Boh m’o# (dt)	1. Háöu (ngæåìi)
                // TODO nhieu nghia, ko vi du
                pattern = Pattern.compile("([\\u0020-\\u007E\\u2019\\u00C0-\\u00FC]+)[\\u0020\\.]+\\(([\\u0020\\u0040-\\u007E\\u2019\\u00C0-\\u00FC]+)\\)\\s*\\t\\d[\\u0020\\.]+([\\u0020-\\u0039\\u003C-\\u007E\\u2019\\u00C0-\\u00FC]+)[\\u0020\\.]+");
                matcher = pattern.matcher(rawInput.get(currentPos));
                if (matcher.matches()) {
                    catched = true;

                    word = new JSONObject();
                    wordArray.put(word);
                    wordTypes = new JSONArray();
                    word.put(WORDTYPES, wordTypes);
                    meanings = new JSONArray();

                    pushAttrToWord(matcher, word, wordTypes, meanings);

                    currentPos++;
                    continue;
                }
                // Yæh yæp (tt)	1. Cháûm chaûp. Ãbat yæh yæp: Âi cháûm chaûp.
                //Bruà mdrao mgu\n (dt)  	1. Y tãú. Knå\ng bruà mdrao mgu\n: Trung tám y tãú.
                //He\ (phuû tæì) 	1. Âaî (coï yï khàóng âënh). Káo mà he\ bruà: Täi âaî laìm viãûc.
                //TODO nhieu nghia, co the co nhieu vi du
                pattern = Pattern.compile("([\\u0020-\\u007E\\u2019\\u00C0-\\u00FC]+)[\\u0020\\.]+\\(([\\u0020\\u0040-\\u007E\\u2019\\u00C0-\\u00FC]+)\\)\\s*\\t\\d[\\u0020\\.]+([\\u0020-\\u007E\\u2019\\u00C0-\\u00FC]+)\\.\\s*((?:[\\u0020-\\u007E\\u2019\\u00C0-\\u00FC]+:[\\u0020-\\u007E\\u2019\\u00C0-\\u00FC]+[;\\.\\u0020\\t]+)*)");
                matcher = pattern.matcher(rawInput.get(currentPos));
                if (matcher.matches()) {
                    catched = true;

                    word = new JSONObject();
                    wordArray.put(word);
                    wordTypes = new JSONArray();
                    word.put(WORDTYPES, wordTypes);
                    meanings = new JSONArray();

                    pushAttrToWord(matcher, word, wordTypes, meanings);

                    currentPos++;
                    continue;
                }


                //            2. Heïo. Am^ khua ama yæ\: Cha giaì meû giaì (heïo). TODO: previous wordtype, 1 meaning, 0 or multiple samples
                //                                	2. ÁÚp. R^ng mnuih [uän sang jing [uän: Chiãu dán láûp áúp.
                // 	2. Giai âoaûn (thåìi kyì).

                pattern = Pattern.compile("\\s*\\t*\\d[\\u0020\\.]+([\\u0020-\\u002D\\u002F-\\u007E\\u2019\\u00C0-\\u00FC]+)[\\.\\s]+((?:[\\u0020-\\u007E\\u2019\\u00C0-\\u00FC]+:[\\u0020-\\u007E\\u2019\\u00C0-\\u00FC]+[;\\.\\u0020\\t]+)*)");// nhieu nghia, co the co nhieu vi du
                matcher = pattern.matcher(rawInput.get(currentPos));
                if (matcher.matches()) {

                    catched = true;

                    try {
                        JSONObject meaning = new JSONObject();
                        meanings.put(meaning);

                        meaning.put(VIETWORD, matcher.group(1));

                        if (matcher.group(2) != null) {
                            String examples = matcher.group(2);
                            String[] arr = examples.split(";");

                            JSONArray sampleArray = new JSONArray();
                            for (String ex : arr) {
                                String[] samplePair = ex.split(":");
                                if (matcher.matches()) {
                                    JSONObject aSample = new JSONObject();
                                    aSample.put(SAMPLE_EDE, samplePair[0]);
                                    aSample.put(SAMPLE_VN, samplePair[1]);
                                    sampleArray.put(aSample);
                                }
                            }
                            meaning.put(SAMPLES, sampleArray);
                        }
                    } catch (Exception e) {
                        currentPos++;
                        continue;

                    }
                    currentPos++;
                    continue;
                }


//            Bi	1. (lt) Coìn. Káo nao mà bruà bi anak káo dã nao sang hrà: Täi âi laìm coìn con täi âi hoüc.TODO: Multiple meaning at first line. There is 1 or multiple samples

                pattern = Pattern.compile("([\\u0020-\\u007E\\u2019\\u00C0-\\u00FC]+)\\t\\d[\\u0020\\.]+\\(([\\u0040-\\u007E\\u2019\\u00C0-\\u00FC]+)\\)[\\u0020\\.]*([\\u0020-\\u002D\\u002F-\\u007E\\u2019\\u00C0-\\u00FC]+)[\\u0020\\.]+((?:[\\u0020-\\u007E\\u2019\\u00C0-\\u00FC]+:[\\u0020-\\u007E\\u2019\\u00C0-\\u00FC]+[;\\.\\u0020]?)+)*");// nhieu nghia, co the co nhieu vi du
                matcher = pattern.matcher(rawInput.get(currentPos));
                if (matcher.matches()) {
                    catched = true;

                    word = new JSONObject();
                    wordArray.put(word);
                    wordTypes = new JSONArray();
                    word.put(WORDTYPES, wordTypes);
                    meanings = new JSONArray();

                    pushAttrToWord(matcher, word, wordTypes, meanings);

                    currentPos++;
                    continue;
                }


//            Ai ãwa (dt) (tt)	Khê thãú, håi sæïc. Phung l^ng kahan rue# nao ho\ng bo\ ai ãwa: Âoaìn quán bæåïc âi âáöy khê thãú; Amáo lo\ máo ai ãwa: Khäng coìn håi sæïc. TODO: Multiple wordtype with the same meaning. There is 1 or multiple samples

//            Ale\, Alue# (âgt)	Nhaí.  Ale\ djah ãhàng: Nhaí baî tráöu. TODO: Multiple edeword with the same wordtype and meaning at first line. There is 1 or multiple samples


                if (!catched) {

                    System.out.print(missMatched++ + " Lines are not matched--");
                    System.out.print(line);
                    System.out.println("");
                }


            } catch (Exception e) {
//                e.printStackTrace();
                System.out.print(missMatched++ + " Lines are error--");
                System.out.println(line);
            }

            currentPos++;

        }
        return wordArray;

    }

    private void pushAttrToWord(Matcher matcher, JSONObject word, JSONArray wordTypes, JSONArray meanings) throws JSONException {
        word.put(EDEWORD, matcher.group(1));

        JSONObject wordType = new JSONObject();
        wordTypes.put(wordType);

        wordType.put(TYPE, matcher.group(2));
        wordType.put(MEANINGS, meanings);

        JSONObject meaning = new JSONObject();
        meanings.put(meaning);

        meaning.put(VIETWORD, matcher.group(3));

        try {
            String examples = matcher.group(4);
            String[] arr = examples.split(";");

            JSONArray sampleArray = new JSONArray();
            for (String ex : arr) {
                String[] samplePair = ex.split(":");
                if (matcher.matches()) {
                    JSONObject aSample = new JSONObject();
                    aSample.put(SAMPLE_EDE, samplePair[0]);
                    aSample.put(SAMPLE_VN, samplePair[1]);
                    sampleArray.put(aSample);
                }
            }
            meaning.put(SAMPLES, sampleArray);
        } catch (IndexOutOfBoundsException e) {

//            System.out.println("Not exist example");
        }
    }

//    public static void main(String[] args) {
//
//        RawDataHandler handler = new RawDataHandler();
//        ArrayList<String> strings = handler.readRaw(null);
////        for (String line : strings) {
////            System.out.println(line);
////        }
//
//        JSONArray jsonArray = handler.parseRawToJson(strings);
//
//        System.out.println(jsonArray.toString());
//
//    }
}
